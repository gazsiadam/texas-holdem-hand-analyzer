package org.virgo.gadam.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.virgo.gadam.model.Card;
import org.virgo.gadam.model.Hand;
import org.virgo.gadam.model.Suit;

/**
 * @author Gazsi Adam
 */
public class TexasHoldemPokerHandAnalyzer extends AbstractPokerHandAnalyzer {

	@Override
	public Hand getHighestHand(List<Card> cards) {
		return getHand(cards).stream().max(Comparator.naturalOrder()).get();
	}

	@Override
	public List<Hand> getHand(List<Card> cards) {
		if (cards == null || cards.size() != 5) {
			throw new RuntimeException("The list should contain exactly 5 cards");
		}

		List<Hand> hand = new ArrayList<>();
		Collections.sort(cards, (c1, c2) -> c1.getValue().compareTo(c2.getValue()));
		List<Integer> cardValueList = cards.stream().map(c -> c.getValue()).collect(Collectors.toList());

		boolean isStraight = isStraight(cardValueList);
		boolean isFlush = isFlush(cards);

		// straight, straight flush, royal flush
		findStraightOrFlushRelatedHands(cards, hand, isStraight, isFlush);

		// if hand contains a straight there's no point verifying it for pairs
		if (isStraight) {
			return hand;
		}

		// pair, double pair, three of a kind, four of a kind
		findPairRelatedHands(cardValueList, hand);

		// high hand
		if (hand.isEmpty()) {
			hand.add(Hand.HighHand);
		} else if (hand.contains(Hand.OnePair) && hand.contains(Hand.ThreeOfAKind)) {
			// full house
			hand.add(Hand.FullHouse);
		}

		return hand;
	}

	private void findStraightOrFlushRelatedHands(List<Card> cards, List<Hand> hand, boolean isStraight,
			boolean isFlush) {
		if (isFlush) {
			hand.add(Hand.Flush);
		}

		if (isStraight) {
			if (isFlush) {
				int maxValue = cards.get(cards.size() - 1).getValue();
				if (maxValue == ROYAL_FLUSH_MAX_VALUE) {
					hand.add(Hand.RoyalFlush);
				}
				hand.add(Hand.StraightFlush);
			}
			hand.add(Hand.Straight);
		}
	}

	private void findPairRelatedHands(List<Integer> cardValueList, List<Hand> hand) {
		Map<Integer, Integer> valueMap = cardValueList.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.reducing(0, e -> 1, Integer::sum)));

		valueMap.entrySet().stream().forEach(v -> {
			if (v.getValue() > 3) {
				hand.add(Hand.FourOfAKind);
			} else if (v.getValue() > 2) {
				hand.add(Hand.ThreeOfAKind);
			} else if (v.getValue() == 2) {
				if (hand.contains(Hand.OnePair)) {
					hand.remove(Hand.OnePair);
					hand.add(Hand.TwoPairs);
				} else {
					hand.add(Hand.OnePair);
				}
			}
		});
	}

	private boolean isFlush(List<Card> cards) {
		Map<Suit, Integer> shapes = cards.stream().map(c -> c.getShape())
				.collect(Collectors.groupingBy(Function.identity(), Collectors.reducing(0, e -> 1, Integer::sum)));

		return shapes.size() == 1 ? true : false;
	}

	private boolean isStraight(List<Integer> cards) {
		Set<Integer> orderedSet = new HashSet<>(cards);

		if (orderedSet.size() < 5) {
			return false;
		} else if (orderedSet.stream().max(Comparator.naturalOrder()).get()
				- orderedSet.stream().min(Comparator.naturalOrder()).get() != 4) {
			// max - min = 4 -> it's a straight
			return false;
		}

		return true;
	}
}
