package org.virgo.gadam.controller;

import java.util.List;

import org.virgo.gadam.model.Card;
import org.virgo.gadam.model.Hand;

public abstract class AbstractPokerHandAnalyzer {

	protected static final int ROYAL_FLUSH_MAX_VALUE = 14;

	/**
	 * Analyze the given cards and verify which "hands" can be composed from the
	 * given cards The list should contain exactly 5 cards
	 * 
	 * @param cards
	 *            - list of cards
	 * @return list of possible hands
	 */
	public abstract List<Hand> getHand(List<Card> cards);

	/**
	 * Analyze the given cards and verify which is the possible highest "hand"
	 * which can be composed from the given cards The list should contain
	 * exactly 5 cards
	 * 
	 * @param cards
	 *            - list of cards
	 * @return the highest possible hand
	 */
	public abstract Hand getHighestHand(List<Card> cards);

}