package org.virgo.gadam.app;

import java.util.ArrayList;
import java.util.List;

import org.virgo.gadam.controller.TexasHoldemPokerHandAnalyzer;
import org.virgo.gadam.model.Card;
import org.virgo.gadam.model.Suit;

public class Main {
	public static void main(String[] args) {
		List<Card> cards = createFourOfAKindCards();
		TexasHoldemPokerHandAnalyzer alg = new TexasHoldemPokerHandAnalyzer();

		System.out.println("Pair: " + alg.getHand(createPairCards()));
		System.out.println("Four of a kind: " + alg.getHand(createFourOfAKindCards()));

	}

	private static List<Card> createPairCards() {
		List<Card> l = new ArrayList<>(5);
		l.add(new Card(Suit.CLUB, 1));
		l.add(new Card(Suit.DIAMOND, 1));
		l.add(new Card(Suit.HEART, 2));
		l.add(new Card(Suit.SPADE, 3));
		l.add(new Card(Suit.SPADE, 4));

		return l;
	}

	private static List<Card> createFourOfAKindCards() {
		List<Card> l = new ArrayList<>(5);
		l.add(new Card(Suit.CLUB, 1));
		l.add(new Card(Suit.DIAMOND, 1));
		l.add(new Card(Suit.HEART, 1));
		l.add(new Card(Suit.SPADE, 1));
		l.add(new Card(Suit.SPADE, 2));

		return l;
	}
}