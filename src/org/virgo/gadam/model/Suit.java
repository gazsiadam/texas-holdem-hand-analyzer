package org.virgo.gadam.model;

import lombok.Getter;

public enum Suit {
	SPADE("S"), HEART("H"), DIAMOND("D"), CLUB("C");

	@Getter
	private String shape;

	private Suit(String s) {
		this.shape = s;
	}
}
