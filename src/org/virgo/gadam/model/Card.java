package org.virgo.gadam.model;

import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Card implements Comparable<Card> {

	Suit shape;
	Integer value;

	@Override
	public int compareTo(Card o) {
		if (Objects.isNull(o)) {
			return 1;
		}

		return o.getValue().compareTo(this.value);
	}

	@Override
	public String toString() {
		return "[" + shape.getShape() + value + "]";
	}

}
