package org.virgo.gadam.model;

public enum Hand {
	HighHand, OnePair, TwoPairs, ThreeOfAKind, Straight, Flush, FullHouse, FourOfAKind, StraightFlush, RoyalFlush;
}
