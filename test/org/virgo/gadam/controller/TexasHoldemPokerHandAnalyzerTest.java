package org.virgo.gadam.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.virgo.gadam.controller.TexasHoldemPokerHandAnalyzer;
import org.virgo.gadam.model.Card;
import org.virgo.gadam.model.Hand;
import org.virgo.gadam.model.Suit;

public class TexasHoldemPokerHandAnalyzerTest {

	private TexasHoldemPokerHandAnalyzer pokerHandAnalyzer = new TexasHoldemPokerHandAnalyzer();

	@Test // royal flush = straight + flush + straight flush
	public void testRoyalFlush() {
		List<Card> cards = new ArrayList<>(5);
		cards.add(new Card(Suit.CLUB, 10));
		cards.add(new Card(Suit.CLUB, 11));
		cards.add(new Card(Suit.CLUB, 12));
		cards.add(new Card(Suit.CLUB, 13));
		cards.add(new Card(Suit.CLUB, 14));

		List<Hand> hand = this.pokerHandAnalyzer.getHand(cards);
		assertEquals(4, hand.size());
		assertTrue(hand.contains(Hand.RoyalFlush));
		assertTrue(hand.contains(Hand.StraightFlush));
		assertTrue(hand.contains(Hand.Straight));
		assertTrue(hand.contains(Hand.Flush));

		assertEquals(Hand.RoyalFlush, this.pokerHandAnalyzer.getHighestHand(cards));
	}

	@Test // straight flush is a straight and a flush
	public void testStraightFlush() {
		List<Card> cards = new ArrayList<>(5);
		cards.add(new Card(Suit.CLUB, 9));
		cards.add(new Card(Suit.CLUB, 10));
		cards.add(new Card(Suit.CLUB, 11));
		cards.add(new Card(Suit.CLUB, 12));
		cards.add(new Card(Suit.CLUB, 13));

		List<Hand> hand = this.pokerHandAnalyzer.getHand(cards);
		assertEquals(3, hand.size());
		assertTrue(hand.contains(Hand.StraightFlush));
		assertTrue(hand.contains(Hand.Straight));
		assertTrue(hand.contains(Hand.Flush));

		assertEquals(Hand.StraightFlush, this.pokerHandAnalyzer.getHighestHand(cards));
	}

	@Test
	public void testFlush() {
		List<Card> cards = new ArrayList<>(5);
		cards.add(new Card(Suit.CLUB, 1));
		cards.add(new Card(Suit.CLUB, 3));
		cards.add(new Card(Suit.CLUB, 4));
		cards.add(new Card(Suit.CLUB, 5));
		cards.add(new Card(Suit.CLUB, 6));

		List<Hand> hand = this.pokerHandAnalyzer.getHand(cards);
		assertEquals(1, hand.size());
		assertTrue(hand.contains(Hand.Flush));

		assertEquals(Hand.Flush, this.pokerHandAnalyzer.getHighestHand(cards));
	}

	@Test
	public void testStraight() {
		List<Card> cards = new ArrayList<>(5);
		cards.add(new Card(Suit.DIAMOND, 2));
		cards.add(new Card(Suit.CLUB, 3));
		cards.add(new Card(Suit.CLUB, 4));
		cards.add(new Card(Suit.CLUB, 5));
		cards.add(new Card(Suit.CLUB, 6));

		List<Hand> hand = this.pokerHandAnalyzer.getHand(cards);
		assertEquals(1, hand.size());
		assertTrue(hand.contains(Hand.Straight));

		assertEquals(Hand.Straight, this.pokerHandAnalyzer.getHighestHand(cards));
	}

	@Test
	public void testFourOfAKind() {
		List<Card> cards = new ArrayList<>(5);
		cards.add(new Card(Suit.DIAMOND, 1));
		cards.add(new Card(Suit.CLUB, 1));
		cards.add(new Card(Suit.HEART, 1));
		cards.add(new Card(Suit.SPADE, 1));
		cards.add(new Card(Suit.CLUB, 6));

		List<Hand> hand = this.pokerHandAnalyzer.getHand(cards);
		assertEquals(1, hand.size());
		assertTrue(hand.contains(Hand.FourOfAKind));

		assertEquals(Hand.FourOfAKind, this.pokerHandAnalyzer.getHighestHand(cards));
	}

	@Test
	public void testFullHouse() {
		List<Card> cards = new ArrayList<>(5);
		cards.add(new Card(Suit.DIAMOND, 1));
		cards.add(new Card(Suit.CLUB, 1));
		cards.add(new Card(Suit.HEART, 1));
		cards.add(new Card(Suit.SPADE, 6));
		cards.add(new Card(Suit.CLUB, 6));

		List<Hand> hand = this.pokerHandAnalyzer.getHand(cards);
		assertEquals(3, hand.size());
		assertTrue(hand.contains(Hand.FullHouse));
		assertTrue(hand.contains(Hand.ThreeOfAKind));
		assertTrue(hand.contains(Hand.OnePair));

		assertEquals(Hand.FullHouse, this.pokerHandAnalyzer.getHighestHand(cards));
	}

	@Test
	public void testThreeOfAKind() {
		List<Card> cards = new ArrayList<>(5);
		cards.add(new Card(Suit.DIAMOND, 1));
		cards.add(new Card(Suit.CLUB, 1));
		cards.add(new Card(Suit.HEART, 1));
		cards.add(new Card(Suit.SPADE, 7));
		cards.add(new Card(Suit.CLUB, 6));

		List<Hand> hand = this.pokerHandAnalyzer.getHand(cards);
		assertEquals(1, hand.size());
		assertTrue(hand.contains(Hand.ThreeOfAKind));

		assertEquals(Hand.ThreeOfAKind, this.pokerHandAnalyzer.getHighestHand(cards));
	}

	@Test
	public void testTwoPairs() {
		List<Card> cards = new ArrayList<>(5);
		cards.add(new Card(Suit.DIAMOND, 1));
		cards.add(new Card(Suit.CLUB, 1));
		cards.add(new Card(Suit.HEART, 7));
		cards.add(new Card(Suit.SPADE, 7));
		cards.add(new Card(Suit.CLUB, 6));

		List<Hand> hand = this.pokerHandAnalyzer.getHand(cards);
		assertEquals(1, hand.size());
		assertTrue(hand.contains(Hand.TwoPairs));

		assertEquals(Hand.TwoPairs, this.pokerHandAnalyzer.getHighestHand(cards));
	}

	@Test
	public void testPair() {
		List<Card> cards = new ArrayList<>(5);
		cards.add(new Card(Suit.DIAMOND, 1));
		cards.add(new Card(Suit.CLUB, 2));
		cards.add(new Card(Suit.HEART, 7));
		cards.add(new Card(Suit.SPADE, 7));
		cards.add(new Card(Suit.CLUB, 6));

		List<Hand> hand = this.pokerHandAnalyzer.getHand(cards);
		assertEquals(1, hand.size());
		assertTrue(hand.contains(Hand.OnePair));

		assertEquals(Hand.OnePair, this.pokerHandAnalyzer.getHighestHand(cards));
	}

	@Test
	public void testHighHand() {
		List<Card> cards = new ArrayList<>(5);
		cards.add(new Card(Suit.DIAMOND, 1));
		cards.add(new Card(Suit.CLUB, 2));
		cards.add(new Card(Suit.HEART, 7));
		cards.add(new Card(Suit.SPADE, 8));
		cards.add(new Card(Suit.CLUB, 6));

		List<Hand> hand = this.pokerHandAnalyzer.getHand(cards);
		assertEquals(1, hand.size());
		assertTrue(hand.contains(Hand.HighHand));

		assertEquals(Hand.HighHand, this.pokerHandAnalyzer.getHighestHand(cards));
	}

	@Test(expected = RuntimeException.class)
	public void testInvalidPreconditionNullHand() {
		this.pokerHandAnalyzer.getHand(null);
	}

	@Test(expected = RuntimeException.class)
	public void testInvalidPreconditionTooFewCards() {
		List<Card> cards = new ArrayList<>(5);
		cards.add(new Card(Suit.DIAMOND, 1));
		cards.add(new Card(Suit.CLUB, 2));
		cards.add(new Card(Suit.HEART, 7));
		cards.add(new Card(Suit.SPADE, 8));

		this.pokerHandAnalyzer.getHand(cards);
	}

	@Test(expected = RuntimeException.class)
	public void testInvalidPreconditionTooManyCards() {
		List<Card> cards = new ArrayList<>(5);
		cards.add(new Card(Suit.DIAMOND, 1));
		cards.add(new Card(Suit.CLUB, 2));
		cards.add(new Card(Suit.HEART, 7));
		cards.add(new Card(Suit.SPADE, 8));
		cards.add(new Card(Suit.SPADE, 8));
		cards.add(new Card(Suit.SPADE, 8));

		this.pokerHandAnalyzer.getHand(cards);
	}

}
